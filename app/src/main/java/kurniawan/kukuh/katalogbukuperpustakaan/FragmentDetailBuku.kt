package kurniawan.kukuh.katalogbukuperpustakaan

import android.app.AlertDialog
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import kotlinx.android.synthetic.main.frag_detail_buku.view.*

class FragmentDetailBuku : Fragment(), View.OnClickListener {

    //Inisialisasi Variabel
    lateinit var thisParent : MainActivity
    lateinit var fragBuku :  FragmentBuku
    lateinit var fragEditBuku : FragmentEditBuku
    lateinit var ft : FragmentTransaction
    lateinit var v : View
    lateinit var dialog : AlertDialog.Builder
    lateinit var db : SQLiteDatabase
    var id_buku : String=""

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btEdit ->{
                val args = Bundle()
                args.putString("kode",id_buku)
                ft = (activity as AppCompatActivity).supportFragmentManager.beginTransaction()
                fragEditBuku.arguments = args
                ft.replace(R.id.frameLayout,fragEditBuku).addToBackStack(null).commit()
            }
            R.id.btHapus ->{
                dialog.setTitle("Konfirmasi").setMessage("Yakin akan menghapus data ini?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnHapusDialog)
                    .setNegativeButton("Tidak",null)
                dialog.show()
            }
            R.id.btKembali->{
                ft = (activity as AppCompatActivity).supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragBuku).commit()
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        db = thisParent.getDBObject()
        dialog = AlertDialog.Builder(thisParent)
        fragBuku = FragmentBuku()
        fragEditBuku = FragmentEditBuku()
        id_buku = arguments!!.getString("kode","")
        v = inflater.inflate(R.layout.frag_detail_buku,container,false)
        v.btEdit.setOnClickListener(this)
        v.btHapus.setOnClickListener(this)
        v.btKembali.setOnClickListener(this)
        return v
    }

    override fun onStart() {
        super.onStart()
        showDetailBuku(id_buku)
    }

    fun showDetailBuku(kode : String){
        v.txKode.setText(kode)
        var sql = "select b.judul, b.pengarang, b.penerbit, k.nama_kategori as kategori, b.halaman, b.sinopsis " +
                "from buku b,kategori k where b.id_kategori=k.id_kategori and kode = '$kode'"
        val c : Cursor = db.rawQuery(sql,null)
        c.moveToFirst()
        v.txJudul.setText(c.getString(c.getColumnIndex("judul")))
        v.txPengarang.setText(c.getString(c.getColumnIndex("pengarang")))
        v.txPenerbit.setText(c.getString(c.getColumnIndex("penerbit")))
        v.txKategori.setText(c.getString(c.getColumnIndex("kategori")))
        v.txHalaman.setText(c.getString(c.getColumnIndex("halaman")))
        v.edSinopsis.setText(c.getString(c.getColumnIndex("sinopsis")))
    }
    val btnHapusDialog = DialogInterface.OnClickListener { dialog, which ->
        hapusDataBuku(id_buku)
        ft = (activity as AppCompatActivity).supportFragmentManager.beginTransaction()
        ft.replace(R.id.frameLayout,fragBuku).commit()
    }
    fun hapusDataBuku(kode: String){
        db.delete("buku","kode = '$kode'",null)
    }
}