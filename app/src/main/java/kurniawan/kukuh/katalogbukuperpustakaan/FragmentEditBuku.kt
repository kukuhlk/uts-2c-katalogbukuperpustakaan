package kurniawan.kukuh.katalogbukuperpustakaan

import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.CursorAdapter
import android.widget.SimpleCursorAdapter
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import kotlinx.android.synthetic.main.frag_edit_buku.*
import kotlinx.android.synthetic.main.frag_edit_buku.view.*

class FragmentEditBuku : Fragment(), View.OnClickListener, AdapterView.OnItemSelectedListener {
    lateinit var thisParent : MainActivity
    lateinit var spAdapter: SimpleCursorAdapter
    lateinit var fragDetailBuku : FragmentDetailBuku
    lateinit var ft : FragmentTransaction
    lateinit var v : View
    lateinit var dialog : AlertDialog.Builder
    lateinit var db : SQLiteDatabase
    var namaKategori : String=""
    var id_buku : String=""
    var mArrayList = ArrayList<String>()

    override fun onNothingSelected(parent: AdapterView<*>?) {
        spKategori.setSelection(0,true)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val c : Cursor = spAdapter.getItem(position) as Cursor
        namaKategori = c.getString(c.getColumnIndex("_id"))
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        db = thisParent.getDBObject()
        dialog = AlertDialog.Builder(thisParent)
        fragDetailBuku = FragmentDetailBuku()
        id_buku = arguments!!.getString("kode","")
        v = inflater.inflate(R.layout.frag_edit_buku,container,false)
        v.spKategori.onItemSelectedListener = this
        v.btnEdit.setOnClickListener(this)
        v.btKembali.setOnClickListener(this)
        return v
    }
    override fun onStart() {
        super.onStart()
        showDataBuku(id_buku)
        showDataKategori()
    }

    fun showDataBuku(kode : String){
        v.edKode.setText(kode)
        var sql = "select b.judul, b.pengarang, b.penerbit, k.nama_kategori as kategori, b.halaman, b.sinopsis " +
                "from buku b,kategori k where b.id_kategori=k.id_kategori and kode = '$kode'"
        val c : Cursor = db.rawQuery(sql,null)
        c.moveToFirst()
        v.edJudul.setText(c.getString(c.getColumnIndex("judul")))
        v.edPengarang.setText(c.getString(c.getColumnIndex("pengarang")))
        v.edPenerbit.setText(c.getString(c.getColumnIndex("penerbit")))
        namaKategori = c.getString(c.getColumnIndex("kategori"))
        v.spKategori.setSelection(getIndex(v.spKategori,namaKategori))
        v.edHalaman.setText(c.getString(c.getColumnIndex("halaman")))
        v.edSinopsis.setText(c.getString(c.getColumnIndex("sinopsis")))
    }

    fun getIndex(spinner: Spinner, myString: String): Int {
        var a = spinner.count
        var b : String = ""
        for (i in 0 until a) {
            b = mArrayList.get(i)
            if (b.equals(myString, ignoreCase = true)) {
                return i
            }
        }
        return 0
    }

    fun showDataKategori(){
        val c : Cursor = db.rawQuery("select nama_kategori as _id from kategori order by nama_kategori asc",null)
        spAdapter = SimpleCursorAdapter(thisParent,android.R.layout.simple_spinner_item,c,
            arrayOf("_id"), intArrayOf(android.R.id.text1), CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spKategori.adapter = spAdapter
        c.moveToFirst()
        var temp : String = ""
        while (!c.isAfterLast()) {
            temp = c.getString(c.getColumnIndex("_id"))
            mArrayList.add(temp) //add the item
            c.moveToNext()
        }
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnEdit->{
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data yang akan dimasukkan sudah benar?")
                    .setPositiveButton("Ya",btnUpdateDialog)
                    .setNegativeButton("Tidak",null)
                dialog.show()
            }
            R.id.btKembali->{
                val args = Bundle()
                args.putString("kode",id_buku)
                ft = (activity as AppCompatActivity).supportFragmentManager.beginTransaction()
                fragDetailBuku.arguments = args
                ft.replace(R.id.frameLayout,fragDetailBuku).commit()
            }
        }
    }

    val btnUpdateDialog = DialogInterface.OnClickListener { dialog, which ->
        var sql = "select id_kategori as _id from kategori where nama_kategori = '$namaKategori'"
        val c : Cursor = db.rawQuery(sql,null)
        if(c.count>0){
            c.moveToFirst()
            updateDataBuku(v.edKode.text.toString(), v.edJudul.text.toString(),v.edPengarang.text.toString(),v.edPenerbit.text.toString(),
                c.getInt(c.getColumnIndex("_id")),v.edHalaman.text.toString(),v.edSinopsis.text.toString())
            val args = Bundle()
            args.putString("kode",id_buku)
            ft = (activity as AppCompatActivity).supportFragmentManager.beginTransaction()
            fragDetailBuku.arguments = args
            ft.replace(R.id.frameLayout,fragDetailBuku).commit()
        }
    }

    private fun updateDataBuku(kode: String, judul: String, pengarang: String, penerbit: String, kategori: Int, halaman: String, sinopsis: String) {
        var cv : ContentValues = ContentValues()
        cv.put("judul",judul)
        cv.put("pengarang",pengarang)
        cv.put("penerbit",penerbit)
        cv.put("id_kategori",kategori)
        cv.put("halaman",halaman)
        cv.put("sinopsis",sinopsis)
        db.update("buku",cv,"kode = '$kode'",null)
    }
}