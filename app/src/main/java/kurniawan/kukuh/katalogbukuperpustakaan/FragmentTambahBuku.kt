package kurniawan.kukuh.katalogbukuperpustakaan

import android.app.AlertDialog
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.CursorAdapter
import android.widget.SimpleCursorAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import kotlinx.android.synthetic.main.frag_tambah_buku.*
import kotlinx.android.synthetic.main.frag_tambah_buku.view.*

class FragmentTambahBuku : Fragment(), View.OnClickListener, AdapterView.OnItemSelectedListener {
    lateinit var thisParent : MainActivity
    lateinit var spAdapter: SimpleCursorAdapter
    lateinit var ft : FragmentTransaction
    lateinit var fragBuku : FragmentBuku
    lateinit var v : View
    lateinit var dialog : AlertDialog.Builder
    lateinit var db : SQLiteDatabase
    var namaKategori : String=""

    override fun onNothingSelected(parent: AdapterView<*>?) {
        spKategori.setSelection(0,true)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val c : Cursor = spAdapter.getItem(position) as Cursor
        namaKategori = c.getString(c.getColumnIndex("_id"))
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        db = thisParent.getDBObject()
        dialog = AlertDialog.Builder(thisParent)
        fragBuku = FragmentBuku()
        v = inflater.inflate(R.layout.frag_tambah_buku,container,false)
        v.spKategori.onItemSelectedListener = this
        v.btnTambah.setOnClickListener(this)
        v.btKembali.setOnClickListener(this)
        return v
    }

    override fun onStart() {
        super.onStart()
        showDataKategori()
    }

    fun showDataKategori(){
        val c : Cursor = db.rawQuery("select nama_kategori as _id from kategori order by nama_kategori asc",null)
        spAdapter = SimpleCursorAdapter(thisParent,android.R.layout.simple_spinner_item,c,
            arrayOf("_id"), intArrayOf(android.R.id.text1), CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spKategori.adapter = spAdapter
        v.spKategori.setSelection(0)
        namaKategori = c.getString(0)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnTambah->{
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data yang akan dimasukkan sudah benar?")
                    .setPositiveButton("Ya",btnInsertDialog)
                    .setNegativeButton("Tidak",null)
                dialog.show()
            }
            R.id.btKembali->{
                ft = (activity as AppCompatActivity).supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragBuku).commit()
            }
        }
    }

    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        var sql = "select id_kategori as _id from kategori where nama_kategori = '$namaKategori'"
        val c : Cursor = db.rawQuery(sql,null)
        if(c.count>0){
            c.moveToFirst()
            insertDataBuku(v.edKode.text.toString(), v.edJudul.text.toString(),v.edPengarang.text.toString(),v.edPenerbit.text.toString(),
                c.getInt(c.getColumnIndex("_id")),v.edHalaman.text.toString(),v.edSinopsis.text.toString())
            ft = (activity as AppCompatActivity).supportFragmentManager.beginTransaction()
            ft.replace(R.id.frameLayout,fragBuku).commit()
        }
    }

    private fun insertDataBuku(kode: String, judul: String, pengarang: String, penerbit: String, kategori: Int, halaman: String, sinopsis: String) {
        var sql = "insert into buku values (?,?,?,?,?,?,?)"
        db.execSQL(sql, arrayOf(kode,judul,pengarang,penerbit,kategori,halaman,sinopsis))
    }
}