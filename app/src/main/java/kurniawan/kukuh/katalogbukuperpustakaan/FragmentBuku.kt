package kurniawan.kukuh.katalogbukuperpustakaan

import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.CursorAdapter
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import kotlinx.android.synthetic.main.frag_data_buku.*
import kotlinx.android.synthetic.main.frag_data_buku.view.*

class FragmentBuku : Fragment(), View.OnClickListener, AdapterView.OnItemSelectedListener{

    //Inisialisasi Variabel
    lateinit var thisParent : MainActivity
    lateinit var lsAdapter : ListAdapter
    lateinit var spAdapter: SimpleCursorAdapter
    lateinit var fragTambah : FragmentTambahBuku
    lateinit var fragDetailBuku : FragmentDetailBuku
    lateinit var v : View
    lateinit var ft : FragmentTransaction
    var kode : String=""
    var filter : String=""
    lateinit var db : SQLiteDatabase

    //Method Spinner Filter
    override fun onNothingSelected(parent: AdapterView<*>?) {
        spFilter.setSelection(0)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val c : Cursor = spAdapter.getItem(position) as Cursor
        filter = c.getString(c.getColumnIndex("_id"))
    }

    //Method OnClick Button
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnTambah ->{
                ft = (activity as AppCompatActivity).supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragTambah).addToBackStack(null).commit()
            }
            R.id.btnCari ->{
                showDataBuku(edSearch.text.toString())
            }
            R.id.btnFilter ->{
                showFilterBuku(filter)
            }
        }
    }

    //Method Utama
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.frag_data_buku,container,false)
        db = thisParent.getDBObject()
        fragTambah = FragmentTambahBuku()
        fragDetailBuku = FragmentDetailBuku()
        v.btnTambah.setOnClickListener(this)
        v.spFilter.onItemSelectedListener = this
        v.btnCari.setOnClickListener(this)
        v.btnFilter.setOnClickListener(this)
        v.lsBuku.setOnItemClickListener(itemClick)

        return v
    }

    override fun onStart() {
        super.onStart()
        showDataBuku("")
        showDataKategori()
    }

    //Method pendukung
    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c: Cursor = parent.adapter.getItem(position) as Cursor
        kode = c.getString(c.getColumnIndex("_id"))
        val args = Bundle()
        args.putString("kode",kode)
        ft = (activity as AppCompatActivity).supportFragmentManager.beginTransaction()
        fragDetailBuku.arguments = args
        ft.replace(R.id.frameLayout,fragDetailBuku).addToBackStack(null).commit()
    }

    fun showDataBuku(judulBuku : String){
        var sql=""
        if(!judulBuku.trim().equals("")){
            sql = "select b.kode as _id, b.judul, b.pengarang from buku b" +
                    " where b.judul like '%$judulBuku%'"
        }else{
            sql = "select b.kode as _id, b.judul, b.pengarang from buku b" +
                    " order by b.judul asc"
        }
        val c : Cursor = db.rawQuery(sql,null)
        lsAdapter = SimpleCursorAdapter(thisParent,R.layout.item_data_buku,c,
            arrayOf("_id","judul","pengarang"), intArrayOf(R.id.txKode,R.id.txJudul,R.id.txPengarang),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        v.lsBuku.adapter = lsAdapter
    }

    fun showFilterBuku(ktg : String){
        val c : Cursor = db.rawQuery("select id_kategori as _id from kategori where nama_kategori='$ktg'",null)
        if(c.count>0){
            c.moveToFirst()
            var id_kategori : Int = 0
            id_kategori = c.getInt(c.getColumnIndex("_id"))
            Log.d("Output","$id_kategori")
            var sql = "select b.kode as _id, b.judul, b.pengarang from buku b,kategori k" +
                    " where b.id_kategori=k.id_kategori and b.id_kategori='$id_kategori'"
            val c1 : Cursor = db.rawQuery(sql,null)
            lsAdapter = SimpleCursorAdapter(thisParent,R.layout.item_data_buku,c1,
                arrayOf("_id","judul","pengarang"), intArrayOf(R.id.txKode,R.id.txJudul,R.id.txPengarang),
                CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
            v.lsBuku.adapter = lsAdapter
        }
    }

    fun showDataKategori(){
        val c : Cursor = db.rawQuery("select nama_kategori as _id from kategori order by nama_kategori asc",null)
        spAdapter = SimpleCursorAdapter(thisParent,android.R.layout.simple_spinner_item,c,
            arrayOf("_id"), intArrayOf(android.R.id.text1),CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spFilter.adapter = spAdapter
    }
}