package kurniawan.kukuh.katalogbukuperpustakaan

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper(context: Context): SQLiteOpenHelper(context,DB_Name, null, DB_Ver){
    companion object{
        val DB_Name = "perpustakaan"
        val DB_Ver = 1
    }

    override fun onCreate(db: SQLiteDatabase?) {

        val tKategori ="create table kategori(id_kategori integer primary key autoincrement, nama_kategori text not null) "
        val tBuku = "create table buku(kode text primary key, judul text not null, pengarang text not null, penerbit text not null, id_kategori int not null, halaman text not null, sinopsis text not null)"
        val insKategori = "insert into kategori(nama_kategori) values ('Kategori Kamus'),('Kategori Atlas'),('Kategori Ilmiah')"
        db?.execSQL(tKategori)
        db?.execSQL(insKategori)
        db?.execSQL(tBuku)
        
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("Not yet implemented")
    }
}